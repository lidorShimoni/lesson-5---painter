#include "Circle.h"


Circle::Circle(const Point & center, double radius, const string & type, const string & name, unsigned char* color) : center(center), radius(radius), Shape(name, type, color) {}

Circle::~Circle()
{
	delete this;
}

const Point & Circle::getCenter() const {return this->center; }

double Circle::getRadius() const {return this->radius;}

double Circle::getArea() const {return abs(this->radius * this->radius * PI);}

double Circle::getPerimeter() const {return 2 * PI * this->radius;}

void Circle::move(const Point & other)
{
	this->center += other;
}

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), this->_color, 100.0f).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}


