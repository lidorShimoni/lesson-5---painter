#include "Triangle.h"



Triangle::Triangle(const Point & a, const Point & b, const Point & c, const string & type, const string & name, unsigned char* color) : Polygon(name, type, color)
{
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}

Triangle::~Triangle()
{
}

double Triangle::getArea() const
{
	return abs((_points[0].getX() * (_points[1].getY() - _points[2].getY()) + _points[1].getX() * (_points[2].getY() - _points[0].getY()) + _points[2].getX() * (_points[0].getY() - _points[1].getY())) / 2);
}

double Triangle::getPerimeter() const
{
	int perimeter = 0;
	for (int i = 1; i < _points.size(); i++)
		perimeter += _points[i].distance(_points[i-1]);
	return	perimeter + _points[0].distance(_points[_points.size()-1]);
}

void Triangle::move(const Point & other)
{
	for (unsigned int i = 0; i < _points.size(); i++)
		_points[i] += other;
}

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	int userChoice = MessageBoxA(NULL, "Do you want to load a cool triangle?", "Lidor's Painter", MB_YESNO | MB_ICONQUESTION);
	if (userChoice == IDYES)
	{
		unsigned char shit[3][3] = { { 255,0,0},{ 0,255,0 }, {0, 0, 255} };
		board.draw_triangle(_points[0].getX(), _points[0].getY(),
			_points[1].getX(), _points[1].getY(),
			_points[2].getX(), _points[2].getY(), shit[2], shit[0], shit[1], 100.0f).display(disp);
	}
	else
	{
		board.draw_triangle(_points[0].getX(), _points[0].getY(),
			_points[1].getX(), _points[1].getY(),
			_points[2].getX(), _points[2].getY(), this->_color, 100.0f).display(disp);
	}
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
