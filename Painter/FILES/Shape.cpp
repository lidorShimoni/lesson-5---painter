#include "Shape.h"
#include <string>

using namespace std;

Shape::Shape(const string & name, const string & type, unsigned char* color) : _name(name), _type(type) 
{
	this->setColor(color);
	delete(color);
}


void Shape::printDetails() const
{
	cout << this->getType() << "  " << this->getName() << "\t" << this->getArea() << "\t" << this->getPerimeter () << "\t( " << (int)this->_color[0] << ", " << (int)this->_color[1] << ", " << (int)this->_color[2] << " )" << endl;
}

string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}

void Shape::setColor(unsigned char* color)
{
	this->_color[0] = color[0];
	this->_color[1] = color[1];
	this->_color[2] = color[2];
}
