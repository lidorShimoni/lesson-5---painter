#pragma once
#include "Polygon.h"

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name, unsigned char* color);
	virtual ~Triangle();
	
	virtual double getArea() const;
	double getPerimeter() const;
	virtual void move(const Point& other);

	void draw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board);

	void clearDraw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board);

	// override functions if need (virtual + pure virtual)
};