#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Triangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>

enum shapesEnum {CIRCLE = 0, ARROW, TRIANGLE, RECTANGLE};



class Menu
{
public:

	Menu();
	~Menu();


	// Menu options
	void addMenu();
	void modMenu();
	void delShapes();

	boolean emptyCanvas();
	int shapeSelection();
	void frame();
	
private: 
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
	vector<Shape*> shapes;
};

// Shapes Creation
Circle* createCircle();
Arrow* createArrow();
myShapes::Rectangle* createRectangle();
Triangle* createTriangle();



// generic functions
void getShapesPoints(vector<Point*>* points, int pointsNum);
string& shapeName();
unsigned char* getColor();
void setColor(unsigned char* color, char r, char g, char b);
void tryColor(int c);