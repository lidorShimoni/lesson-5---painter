#include "Rectangle.h"


myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const string & type, const string & name, unsigned char* color) : Polygon(name, type, color)
{
	_points.push_back(a);
	_points.push_back(Point(a.getX() + width, a.getY() + length));
}

myShapes::Rectangle::~Rectangle()
{
	delete this;
}

double myShapes::Rectangle::getArea() const {return abs(this->width * this->length);}

double myShapes::Rectangle::getPerimeter() const {return 2 * this->width + 2 * this->length;}

void myShapes::Rectangle::move(const Point & other)
{
	for (int i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] += other;
	}
}


void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{

	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), this->_color, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


