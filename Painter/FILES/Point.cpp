#include "Point.h"

Point::Point(double x, double y) : x(x), y(y) {}

Point::Point(const Point & other) : x(other.getX()), y(other.getY()) {}

Point::~Point()
{
}

Point Point::operator+(const Point & other) const {return Point(this->getX() + other.getX(), this->getY() + other.getY());}

Point & Point::operator+=(const Point & other)
{
	this->x += other.getX();
	this->y += other.getY();
	return *this;
}

double Point::getX() const {return this->x;}

double Point::getY() const {return this->y;}

double Point::distance(const Point & other) const {return sqrt(pow(this->getX() - other.getX(), 2) + pow(this->getY() - other.getY(), 2));}
