#include "Menu.h"
#include "Rectangle.h"

enum choice { MOVE = 0, SETCOLOR, DETAILS, REMOVE};
enum color {BLACK = 0, WHITE, RED, LIME, BLUE, YELLOW, CYAN, MAGENTA, SILVER, GRAY, MAROON, OLIVE, GREEN, PURPLE, TEAL, NAVY};
using namespace std;

Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(1920, 1080, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint", 3, true);
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

void Menu::addMenu()
{
	int selection = -1;
	while (true)
	{
		system("cls");
		cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle." << endl;
		cin >> selection;
		switch (selection)
		{
		case CIRCLE:
			this->shapes.push_back(createCircle()); 
			return;

		case ARROW:
			this->shapes.push_back(createArrow());
			return;

		case TRIANGLE:
			this->shapes.push_back(createTriangle());
			//if()
			return;

		case RECTANGLE:
			this->shapes.push_back(createRectangle());
			return;

		default:
			continue;
		}
	}
}

void Menu::modMenu()
{
	int choice, shape = this->shapeSelection();
	do
	{
		cout << "Enter 0 to move the shape\nEnter 1 to change color.\nEnter 2 to get its details.\nEnter 3 to remove the shape." << endl;
		cin >> choice;
		switch (choice)
		{
		case MOVE:
			int x, y;
			cout << "Please enter the X moving scale: ";
			cin >> x;
			cout << "Please enter the Y moving scale: ";
			cin >> y;
			this->shapes[shape]->clearDraw(*this->_disp, *this->_board);
			this->shapes[shape]->move(Point(x, y));
			break;
		case SETCOLOR:
			this->shapes[shape]->setColor(getColor());
			break;
		case DETAILS:
			this->shapes[shape]->printDetails();
			system("pause");
			break;
		case REMOVE:
			this->shapes[shape]->clearDraw(*this->_disp, *this->_board);
			delete(this->shapes[shape]);
			this->shapes.erase(this->shapes.begin() + shape);
			break;
		default:
			system("cls");
			continue;
		}
	} while (choice > 3 || choice < 0);
}

int Menu::shapeSelection()
{
	int choice;
	do
	{
		system("cls");
		for (unsigned int i = 0; i < this->shapes.size(); i++)
		{
			cout << "Enter " << i << " for " << this->shapes[i]->getName() << "(" << this->shapes[i]->getType() << ")" << endl;
		}
		cin >> choice;
	} while (choice >= this->shapes.size() || choice < 0);
	return choice;
}


void Menu::delShapes()
{
	for (int i = this->shapes.size()-1; i >= 0; i--)
	{
		this->shapes[i]->clearDraw(*this->_disp, *this->_board);
		delete(this->shapes[i]);
		this->shapes.pop_back();
	}
}


// returns True if the canvas is empty of this->shapes
boolean Menu::emptyCanvas()
{
	return this->shapes.empty();
}

void Menu::frame()
{
	for (unsigned int i = 0; i < this->shapes.size(); i++)
	{
		this->shapes[i]->clearDraw(*this->_disp, *this->_board);
		this->shapes[i]->draw(*this->_disp, *this->_board);
	}
}

// generic function to get all the points from the user
void getShapesPoints(vector<Point*>* points, int pointsNum)
{
	double x, y;
	for (int i = 0; i < pointsNum; i++)
	{
		cout << "Enter the X of point number: " << i + 1 << endl;
		cin >> x;
		cout << "Enter the Y of point number: " << i + 1 << endl;
		cin >> y;
		points->push_back(new Point(x, y));
	}
}

// this function creates an rectangle from user's input
myShapes::Rectangle * createRectangle()
{
	int x, y, width, length;
	cout << "Enter the X of the top left corner:" << endl;
	cin >> x;
	cout << "Enter the Y of the top left corner:" << endl;
	cin >> y;
	cout << "Enter the length of the shape:" << endl;
	cin >> length;
	cout << "Enter the width of th shape:" << endl;
	cin >> width;

	if (width != 0 && length != 0)
	{
		return new myShapes::Rectangle(Point(x, y), length, width, "Rectangle", shapeName(), getColor());
	}
	cout << "Length or Width can't be 0." << endl;
	system("pause");
}

// this function creates an triangle from user's input
Triangle * createTriangle()
{
	vector<Point*>* points = new vector<Point*>;
	getShapesPoints(points, 3);
	Triangle *t = new Triangle(*(*points)[0], *(*points)[1], *(*points)[2], "Triangle", shapeName(), getColor());
	if (t->getArea() != 0)
	{
		return t;
	}
	cout << "area is : " << t->getArea() << endl;
	cout << "The points entered create a line." << endl;
	delete(t); 
	system("pause");
	return 0;
}

// this function creates an circle from user's input
Circle* createCircle()
{
	double x, y, radius;
	cout << "Please enter X:" << endl;
	cin >> x;
	cout << "Please enter Y:" << endl;
	cin >> y;
	cout << "Please enter radius:" << endl;
	cin >> radius;
	return new Circle(Point(x, y), radius, string("Circle"), shapeName(), getColor());
}

// this function creates an arrow from user's input
Arrow * createArrow()
{
	vector<Point*>* points = new vector<Point*>;
	getShapesPoints(points, 2);
	cout << points->size() << endl;
	return new Arrow(*(*points)[0], *(*points)[1], "Arrow", shapeName(), getColor());
	system("pause");
}

// generic function to get shape's name
string& shapeName()
{
	string* name = new string();
	cout << "Enter the name of the shape:" << endl;
	cin >> *name;
	return *name;
}

unsigned char* getColor()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	unsigned char* color = new unsigned char[3];
	int selection;
	int txtclr[] = { 0, 255, 204, 170, 17, 238, 187, 221, 119, 136, 68, 102, 34, 85, 51, 153 };
	string clr[] = {"Black", "White", "Red", "Lime", "Blue", "Yellow", "Cyan", "Magenta", "Silver", "Gray", "Maroon", "Olive", "Green", "Purple", "Teal", "Navy"};
	do
	{
		cout << "Enter color from the list below:" << endl;
		for (int i = 0; i < 16; i++)
		{
			tryColor(txtclr[i]);
			cout << i << " - " << clr[i] << endl;
		}
		cout << "color: ";
		cin >> selection;
		switch (selection)
		{
		case BLACK:
			setColor(color, 0, 0, 0);
			break;
		case WHITE:
			setColor(color, 255, 255, 255);
			break;
		case RED:
			setColor(color, 255, 0, 0);
			break;
		case LIME:
			setColor(color, 0, 255, 0);
			break;
		case BLUE:
			setColor(color, 0, 0, 255);
			break;
		case YELLOW:
			setColor(color, 255, 255, 0);
			break;
		case CYAN:
			setColor(color, 0, 255, 255);
			break;
		case MAGENTA:
			setColor(color, 255, 0, 255);
			break;
		case SILVER:
			setColor(color, 192, 192, 192);
			break;
		case GRAY:
			setColor(color, 128, 128, 128);
			break;
		case MAROON:
			setColor(color, 128, 0, 0);
			break;
		case OLIVE:
			setColor(color, 128, 128, 0);
			break;
		case GREEN:
			setColor(color, 0, 128, 0);
			break;
		case PURPLE:
			setColor(color, 128, 0, 128);
			break;
		case TEAL:
			setColor(color, 0, 128, 128);
			break;
		case NAVY:
			setColor(color, 0, 0, 128);
			break;
		default:
			system("cls");
			continue;
		}
	} while (selection > NAVY || selection < BLACK);
	return color;
}

void setColor(unsigned char* color, char r, char g, char b)
{
	color[0] = r;
	color[1] = g;
	color[2] = b;
}

void tryColor(int c)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, c);
	cout << "color";
	SetConsoleTextAttribute(hConsole, 15);
	cout << "   ";
}
