#include "Arrow.h"



Arrow::Arrow(const Point & a, const Point & b, const string & type, const string & name, unsigned char* color) : _p1(a), _p2(b), Shape(name, type, color){}

Arrow::~Arrow()
{
	delete this;
}

double Arrow::getArea() const {return 0;}

double Arrow::getPerimeter() const {return abs(this->_p1.distance(this->_p2));}

void Arrow::move(const Point & other)
{
	this->_p1 += other;
	this->_p2 += other;
}

void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), this->_color, 100.0f).display(disp);
}

void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}


