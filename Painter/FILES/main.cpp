#include "Menu.h"

enum selectionEnum {ADD = 0, MOD, DELETE_ALL, EXIT};

void printMainMenu();

int main()
{
	MessageBoxA(NULL, "HI,\nThis painter was made by Lidor Shimoni with pain and suffering. please do not copy, this shit is useless.", "Warning! I think...?", MB_ICONEXCLAMATION);
	MessageBoxA(NULL, "Dont freak out, the painter will go full screen now...", "get ready", MB_ICONEXCLAMATION);
	Menu* menu = new Menu();
	int selection;
	do {
		selection = -1;
		menu->frame();
		system("cls");
		printMainMenu();
		cin >> selection;
		switch (selection)
		{
		case ADD:
			menu->addMenu();
			break;

		case MOD:
			if (menu->emptyCanvas())
				continue;
			menu->modMenu();
			break;

		case DELETE_ALL:
			menu->delShapes();
			break;

		default:
			continue;
		}
		
	} while (selection != EXIT);
}

void printMainMenu()
{
	cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit." << endl;
}

